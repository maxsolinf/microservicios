from flask_wtf import FlaskForm as Form
from flask_wtf import RecaptchaField
from wtforms import (StringField,
                     PasswordField,
                     BooleanField,
                     SelectField,
                     RadioField,
                     )
from wtforms.validators import DataRequired, Length, EqualTo, URL, Email
from .models import mongo, User, Role
from wtforms.fields.html5 import EmailField


class LoginForm(Form):
    username = StringField('Username', [DataRequired(), Length(max=40)])
    password = PasswordField('Password', [DataRequired(), ])
    remember = BooleanField('Recuerda me')

    def validate(self):
        check_validate = super(LoginForm, self).validate()

        # si no pasa la validacion
        if not check_validate:
            return False

        # Existe el Usuario?
        user = User.objects(username=self.username.data.lower()).first()
        if not user:
            self.username.errors.append("Usuario o Password invalido.")
            return False

        # es el mismo Password?
        if not user.check_password(self.password.data):
            self.username.errors.append('Usuarion o Password invalido.')
            return False

        return True


class RegisterForm(Form):
    username = StringField('Usuario',
                           [DataRequired("Por favor introduce un nombre de usuario"),
                            Length(max=40)])
    password = PasswordField('Password',
                             [DataRequired('Por favor introduce un password'),
                              Length(min=8, max=18)])
    confirm = PasswordField('Repite Password',
                            [DataRequired('Este campo no es opcional'),
                             EqualTo('password')])
    email = EmailField('Email',
                       [DataRequired('Por favor introduce un email'),
                        Email('Por favor introduce un Email Valido')])
    recaptcha = RecaptchaField()

    def validate(self):
        check_validate = super(RegisterForm, self).validate()

        # si no pasa la validacion
        if not check_validate:
            return False

        # buscamos a ver si existe ese usuario
        user = User.objects(username=self.username.data)
        # buscamos emails iguales
        emailer = User.objects(email=self.email.data)

        if user:
            self.username.errors.append("Ya existe un usuario con ese nombre.")
            return False

        if emailer:
            self.email.errors.append("Ya existe un usuario con ese email.")
            return False

        return True


radio_select = [(0, 'Delete'), (1, 'Add')]


class AddUserForm(Form):
    usuario = SelectField('Usuario', [DataRequired('Campo necesario')])
    atributo = SelectField('Atributo', [DataRequired('Campo necesario')])
    operacion = RadioField('Operacion', [DataRequired('Campo Reqerido')])
