from flask_login import (LoginManager,
                         current_user,
                         login_user,
                         AnonymousUserMixin)
from flask_bcrypt import Bcrypt
from flask import abort
import functools

bcrypt = Bcrypt()


class AnonymousUser(AnonymousUserMixin):
    def __init__(self):
        self.username = "Invitado"


login_manager = LoginManager()
login_manager.login_view = "auth.login"
login_manager.session_protection = "strong"
login_manager.login_message = "Por favor inicia sesion para acceder"
login_manager.login_message_category = "info"
login_manager.anonymous_user = AnonymousUser


def create_module(app):
    bcrypt.init_app(app)
    login_manager.init_app(app)

    from .controllers import auth_blueprint

    app.register_blueprint(auth_blueprint)


def has_role(name):
    def real_decorator(f):
        def wraps(*args, **kwargs):
            if current_user.has_role(name):
                return f(*args, **kwargs)
            return abort(403, 'El usuario no posee ese role')
        return functools.update_wrapper(wraps, f)
    return real_decorator

def has_roles(name):
    def real_decorator(f):
        def wraps(*args, **kwargs):
            for role in name:
                if current_user.has_role(role):
                    return f(*args, **kwargs)
            return abort(403, 'El usuario no posee ese role')
        return functools.update_wrapper(wraps, f)
    return real_decorator

@login_manager.user_loader
def load_user(userid):
    from .models import User
    return User.objects.get(id=userid)
