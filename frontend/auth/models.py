from .. import mongo
from . import bcrypt, AnonymousUserMixin
import uuid
import datetime


class Role(mongo.Document):
    nombre = mongo.StringField(max_length=20)
    descripcion = mongo.StringField()

    meta = {
        'collection': 'roles',
        'indexes': [
            'nombre'
        ]
    }

    def __repr__(self):
        return f"Role: '{self.nombre}'"


class Pago(mongo.Document):
    fecha = mongo.DateTimeField()
    concepto = mongo.StringField()
    nota = mongo.ListField(mongo.StringField)


class User(mongo.Document):

    username = mongo.StringField(
        required=True,
        max_legth=40,
    )
    password = mongo.BinaryField(
    )
    email = mongo.StringField()
    roles = mongo.ListField(mongo.ReferenceField(Role))
    public_id = mongo.StringField(default=lambda: str(uuid.uuid4()))
    pago = mongo.ListField(mongo.ReferenceField(
        Pago, reverse_delete_rule=mongo.PULL))
    gestiona = mongo.ListField(mongo.ObjectIdField())
    acceso = mongo.ListField(mongo.StringField())
    meta = {
        'collection': 'usuarios',
        'indexes': [
            'username',
            'email',
            'roles',
        ]
    }

    def __repr__(self):
        return f"<Usurio: '{self.username}'>"

    def has_role(self, name):
        """Ubica los roles de cada usuario para controlaraccesos"""
        if isinstance(name, str):
            for role in self.roles:
                if role.nombre == name:
                    return True
        elif isinstance(name, list):
            for item in name:
                for role in self.roles:
                    if role.nombre == item:
                        return True
                    
        return False

    def set_password(self, password):
        self.password = bcrypt.generate_password_hash(password)

    def check_password(self, password):
        return bcrypt.check_password_hash(self.password, password)

    @ property
    def is_authenticated(self):
        if isinstance(self, AnonymousUserMixin):
            return False
        return True

    @ property
    def is_active(self):
        return True

    @ property
    def is_anonymous(self):
        if isinstance(self, AnonymousUserMixin):
            return True
        return False

    def get_id(self):
        return str(self.id)

    @ property
    def activo(self):
        for pago in self.pagos:
            if pago.fecha > datetime.datetime.now():
                return True
            else:
                continue
        return False
