from flask import (Blueprint,
                   jsonify,
                   flash,
                   abort,
                   url_for,
                   render_template,
                   redirect, current_app, request)
from flask_login import (login_user,
                         login_required,
                         current_user,
                         logout_user,
                         )
from werkzeug.local import LocalProxy
import requests

from .models import User, Role

from .forms import LoginForm, RegisterForm, AddUserForm

from . import has_role


auth_blueprint = Blueprint(
    'auth',
    __name__,
    url_prefix='/auth',
    template_folder='../templates/auth',
    static_folder='../static',)


logger = LocalProxy(lambda: current_app.logger)


def roleando():
    defecto = Role.objects(nombre='defecto').first()
    if not defecto:
        defecto = Role(nombre='defecto')
        defecto.save()
        return defecto
    return defecto


@auth_blueprint.route('/')
@login_required
@has_role('admin')
def auth_index():
    print(current_user.username)
    return render_template(
        'user.html'
    )


@auth_blueprint.route('/login', methods=['GET', 'POST'])
def login():
    """Controlador de inicio de sesones"""
    form = LoginForm()
    if form.validate_on_submit():
        username = form.username.data
        username = username.lower()
        user = User.objects(
            username=username).first()  # el primer user
        # logealo y rem
        login_user(user, remember=form.remember.data)
        flash('Haz iniciado Sesion.', category='success')
        # devuelve a
        return redirect(url_for('main.front_page'))

    return render_template('login.html', form=form, current_user=current_user)


@auth_blueprint.route('/logout', methods=['GET', 'POST'])
def logout():
    """Controlador de Cerrar Sessiones"""
    logout_user()
    flash("Haz Deslogeado tu sesion", category='success')
    return redirect(url_for('.auth_index'))


@auth_blueprint.route('/register', methods=['GET', 'POST'])
def register():
    """Controlador de Registro de nuevos Usuarios"""
    print(current_app.config)
    form = RegisterForm()

    if form.validate_on_submit():
        new_user = User(username=form.username.data.lower())
        # hashea el password auto
        new_user.set_password(form.password.data)
        new_user.email = form.email.data                # Guarda el email del usuario
        role = roleando()   # asigna role defecto auto
        new_user.roles.append(role)
        new_user.save()
        flash("Usuario Creado Satisfactoriamente", category='success')
        return redirect(url_for('.login'))              # devuelve a

    return render_template('register.html', form=form)
    # flash("Los nuevos registros se encuentran desactivados, comunicate con el administrador para crear una cuenta", category="danger")
    # return redirect(url_for('main.front_page'))

    
@auth_blueprint.route('/administra', methods=['GET', 'POST'])
def administra():
    users = [(str(indi.id), indi.username)
             for indi in User.objects()
             ]
    roles = [(str(role.id), role.nombre)
             for role in Role.objects()
             ]
    propietarios = [((user.id), user.username)
                    for user in User.objects()
                    if user.has_role('propietario')
                    ]
    gestores = [((user.id), user.username)
                for user in User.objects()
                if user.has_role('gestor')
                ]

    url = 'http://alquiler:5000/api/propietario'
    response = requests.get(url)
    mensaje = response.json()
    items_alquiler = mensaje.get('propietarios')
    alquiler = [(item['_id']['$oid'], item['nombre'] + ' ' + item['apellido'])
                for item in items_alquiler
                if item
                ]
    form = AddUserForm()
    form.usuario.choices = users
    form.atributo.choices = roles

    return render_template('inicio_administra.html', form=form)


# GESTION USUARIOS
@auth_blueprint.route('/administra/roles', methods=['GET', 'POST'])
@login_required
@has_role('admin')
def administra_roles():
    titulo = "Gestion de Roles"
    users = [(str(indi.id), indi.username)
             for indi in User.objects()
             ]
    roles = [(str(role.id), role.nombre)
             for role in Role.objects()
             ]
    form = AddUserForm()
    
    form.usuario.choices = users
    form.atributo.choices = roles
    form.operacion.choices = [('0', 'Delete'), ('1', 'Add')]
    logger.info(form.validate())
    if request.method == 'POST':
        if form.validate():
            logger.info(form.operacion.data)
            if form.operacion.data == '1':
                role = form.atributo.data
                role = Role.objects(id=role).first()
                if not role:
                    flash(f'El Role {form.atributo.data} no existe',
                          category='danger'
                          )
                    return redirect(url_for('administra'))
                usuario = form.usuario.data
                User.objects(id=usuario).update_one(add_to_set__roles=role)
                flash('Operacion ejecutada', category='info')
                return redirect(url_for('.administra'))
            if form.operacion.data == '0':
                role = form.atributo.data
                role = Role.objects(id=role).first()
                if not role:
                    flash(f'El Role {form.atributo.data} no existe',
                          category='danger'
                          )
                    return redirect(url_for('administra'))
                usuario = form.usuario.data
                User.objects(id=usuario).update_one(pull__roles=role)
                flash('Operacion ejecutada (Eliminacion)', category='info')
                return redirect(url_for('.administra'))
            

    return render_template('administra.html', form=form, titulo=titulo)
    

@auth_blueprint.route('/administra/propietarios', methods=['GET', 'POST'])
@login_required
@has_role('admin')
def administra_propietarios():
    titulo = "Gestion de Propietario"
    propietarios = [(str(user.id), user.username)
                    for user in User.objects()
                    if user.has_role('propietario') or user.has_role('gestor')
                    ]
    url = 'http://alquiler:5000/api/propietario'
    response = requests.get(url)
    mensaje = response.json()
    items_alquiler = mensaje.get('propietarios')
    alquiler = [(item['_id']['$oid'], item['nombre'] + ' ' + item['apellido'])
                for item in items_alquiler
                if item
                ]
    form = AddUserForm()
    form.usuario.choices = propietarios
    form.atributo.choices = alquiler
    form.operacion.choices = [('0', 'Delete'), ('1', 'Add')]
    if form.validate_on_submit():
        propietario = {'_id': {'$oid': form.atributo.data}}
        usuario = {'_id': {'$oid': form.usuario.data}}
        data = {'propietario': propietario, 'consultor': usuario}
        url = 'http://alquiler:5000/api/propietario'
        if form.operacion.data == '0':
            operacion = requests.delete(url, json=data)
            mensaje = operacion.json()
            flash(mensaje['mensaje'], category='info')
            return redirect(url_for('.administra'))
        
        if form.operacion.data == '1':
            operacion = requests.put(url, json=data)
            mensaje = operacion.json()
            flash(mensaje['mensaje'], category='info')
            return redirect(url_for('.administra'))

    return render_template('administra.html', form=form, titulo=titulo)

