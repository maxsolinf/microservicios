import time
import qrcode
import datetime
from collections import Counter
import requests
from werkzeug.local import LocalProxy
import io
import jinja2
from flask_weasyprint import HTML, render_pdf
from flask_login import login_required
from flask import (Blueprint,
                   redirect,
                   url_for,
                   flash,
                   render_template,
                   request,
                   abort,
                   session,
                   send_file,
                   Response,
                   current_app, request_started,
                   )
from ..auth import current_user, has_role, AnonymousUser, has_roles
from ..auth.models import User
from .forms import (PropietarioForm, NotaForm,
                    ViviendaForm, InquilinoForm,
                    PagoForm, VisualForm)
from .procesa_datos import procesa_datos, proceso_pagos, mes
from .postproceso import owner, casa
from .tasks import send_mail
import json


alquiler_blueprint = Blueprint(
    'alquiler',
    __name__,
    url_prefix='/alquiler',
    template_folder='../templates/alquiler/',
    static_folder='../static/'
)

logger = LocalProxy(lambda: current_app.logger)


@alquiler_blueprint.before_request
def print_request():
    useragent = request
    logger.info(request.remote_addr)


def qr_generator(info):
    qr = qrcode.QRCode(
        version=1,
        error_correction=qrcode.constants.ERROR_CORRECT_L,
        box_size=5,
        border=4,
    )
    qr.add_data(info)
    qr.make(fit=True)
    img = qr.make_image(fill_color="black", back_color="white")
    with open('./frontend/static/img/qrcode.png', "wb") as f:
        img.save(f)


@alquiler_blueprint.route('/', methods=['GET'])
def admin_home():
    if current_user.is_authenticated:
        if current_user.has_role('admin'):
            flash('Usuario posee permisos de admin.')
            url = 'http://alquiler:5000/api/propietario'
            response = requests.get(url)
            mensaje = response.json()
            session['propietarios'] = mensaje.get('propietarios')

            return render_template('home_alquiler.html',
                                   propietarios=mensaje)

        elif current_user.has_role('gestor'):
            if current_user.gestiona:
                flash('Usuario posee propietarios que gestionar.')
                url = 'http://alquiler:5000/api/propietario'
                data = [{"_id": {"$oid": str(prop)}}
                        for prop in current_user.gestiona]
                response = requests.get(url, json=data)
                mensaje = response.json()
                session['propietarios'] = mensaje.get('propietarios')

                return render_template('home_alquiler.html',
                                       propietarios=mensaje)

            return render_template('home_alquiler.html', propietarios=True)

        elif current_user.has_role('propietario'):
            if current_user.gestiona:
                flash('usuario es propietario.', category="info")
                url = 'http://alquiler:5000/api/propietario'
                data = {"_id": {"$oid": str(current_user.gestiona[0])}}
                response = requests.get(url, json=data)
                mensaje = response.json()
                # logger.info(mensaje)
                session['propietarios'] = [mensaje.get('propietario')]

                return redirect(url_for('.ver_propietario', indice=0))
            flash('Usuario es propietario pero no tiene asignada ninguna propiedad.',
                  category="alert")
        return redirect(url_for("main.front_page"))
    else:
        flash("Usuaro para visualizacion.")
        return redirect(url_for('.inquilino_visualizacion'))


######################## Visualizacion Modelos #######################


@alquiler_blueprint.route('/propietatio/<indice>',
                          methods=['GET', 'POST'])
@login_required
@has_role(['admin', 'gestor', 'propietario'])
def ver_propietario(indice):
    """ Asumiendo que siempre se accede a  este endpoint desde home en caso 
    De que el que acceda sea el propietario optaremos por agregar directamente
    en el home_alquiler session['propietarios'] accediendo por indice nos 
    ahorramos mucha logica y podemos simplificar el endpoint"""
    nota_form = NotaForm()
    pago_form = PagoForm()

    if nota_form.validate_on_submit() and nota_form.comentario.data:
        contenido = f"<p>Comentario: {nota_form.comentario.data}</p>"
        usuario = f"<p>Usuario: {current_user.username}</p>"
        time_epoch = f'<p>Fecha: {time.strftime("%x %X")}</p>'
        string = " \n".join([usuario, time_epoch, contenido])
        propietario = session.get('propietario')
        data = {'propietario': propietario, 'nota': string}
        url = 'http://alquiler:5000/api/propietario'
        response = requests.put(url, json=data)
        mensaje = response.json()
        flash(mensaje.get('mensaje'), category="info")
        return redirect(url_for('.ver_propietario',
                                indice=session['prop_indice'],
                                nota_form=nota_form,
                                pago_form=pago_form))

    if pago_form.validate_on_submit() and pago_form.cantidad.data:
        cantidad = abs(pago_form.cantidad.data)
        mes = pago_form.mes.data
        forma_pago = pago_form.forma_pago.data
        nota = pago_form.nota.data or 'sin comentarios'
        propietario = session['propietario']
        data = {'cantidad': cantidad, 'mes': mes,
                'forma_pago': forma_pago, 'notas': nota,
                'propietario': propietario, 'pago': True,
                }
        url = 'http://alquiler:5000/api/propietario'
        response = requests.put(url, json=data)
        mensaje = response.json()
        usuario = current_user
        fecha = datetime.datetime.now()
        correos = [current_user.email, propietario['email']]
        pago = data
        texto = render_template('email_pago_propietario.html',
                                pago=data, usuario=usuario,
                                propietario=propietario, fecha=fecha)
        fecha = time.strftime("%x %X")
        info = f"""En fecha {fecha} 
se pago la cantidad de {formato_dinero(pago['cantidad'])}, 
de forma { pago['forma_pago']} por el mes de {nombre_mes(pago['mes'])},
a {propietario['nombre'] + ' ' + propietario['apellido']},
impreso por: '{current_user.username}'
       {fecha} 
San Cristobal, Republica Dominicana.  
        """
        qr_generator(info)
        for x in correos:
            data1 = {'correos': x, 'texto': texto}
            send_mail.delay(data1)
        # flash(mensaje.get('mensaje'), category="info")
        return redirect(url_for('.ver_propietario',
                                indice=session['prop_indice'],
                                nota_form=nota_form,
                                pago_form=pago_form))

    indice = int(indice)
    session['prop_indice'] = indice
    propietario = session['propietarios'][indice]
    session['propietario'] = propietario
    url = 'http://alquiler:5000/api/propietario'
    data = propietario
    response = requests.get(url, json=data, timeout=1)
    result = response.json()
    viviendas, notas, pagos, informacion, propietario = owner(result)
    pago_form.nota.data = "Sin comentarios."

    return render_template('home_alquiler.html',
                           propietario=propietario,
                           viviendas=viviendas,
                           nota_form=nota_form,
                           prop_indice=session['prop_indice'],
                           pago_form=pago_form,
                           informacion=informacion,)


@alquiler_blueprint.route('/vivienda/<indice>',
                          methods=['GET', 'POST'])
@login_required
@has_role(['admin', 'gestor', 'propietario'])
def ver_vivienda(indice):
    nota_form = NotaForm()
    if nota_form.validate_on_submit and nota_form.comentario.data:
        contenido = f"<p>Comentario: {nota_form.comentario.data}</p>"
        usuario = f"<p>Usuario: {current_user.username}</p>"
        time_epoch = f'<p>Fecha: {time.strftime("%x %X")}</p>'
        string = " \n".join([usuario, time_epoch, contenido])
        propietario = session.get('propietario')
        vivienda = session.get('viv_indice')
        data = {'propietario': propietario,
                'nota': string, 'vivienda': vivienda}

        url = 'http://alquiler:5000/api/vivienda'
        response = requests.post(url, json=data)
        mensaje = response.json()
        flash(mensaje.get('mensaje'), category="info")
        return redirect(url_for('.ver_vivienda',
                                indice=session['viv_indice'],
                                nota_form=nota_form,
                                ))
    indice = int(indice)
    propietario = session['propietario']
    vivienda = session['viviendas'][indice]
    url = 'http://alquiler:5000/api/vivienda'
    data = {'propietario': propietario, 'indice': indice}
    response = requests.get(url, json=data, timeout=1)
    resultado = response.json()

    session['viv_indice'] = indice
    informacion, vivienda, inquilino = casa(resultado)
    return render_template('vivienda.html',
                           vivienda=vivienda,
                           propietario=propietario,
                           inquilino=inquilino,
                           prop_indice=session.get('prop_indice'),
                           nota_form=nota_form,
                           informacion=informacion,
                           )


@alquiler_blueprint.route('/vivienda/inquilino/',
                          methods=['GET', 'POST'])
@login_required
@has_role(['admin', 'gestor', 'propietario'])
def ver_inquilino():
    form = PagoForm()

    if form.validate_on_submit():
        cantidad = abs(form.cantidad.data)
        mes = form.mes.data
        forma_pago = form.forma_pago.data
        nota = form.nota.data or 'sin comentarios'
        propietario = session['propietario']
        viv_indice = session['viv_indice']
        data = {'cantidad': cantidad, 'mes': mes,
                'forma_pago': forma_pago, 'nota': nota,
                'propietario': propietario, 'indice': viv_indice,
                }
        pay = dict(data)
        pago = pay
        correos = [current_user.email]
        logger.info(correos)
        correos.append(session['inquilino']['email'])
        correos.append(session['propietario']['email'])
        inquilino = session.get('inquilino')
        vivienda = session.get('vivienda')
        usuario = current_user
        fecha = datetime.datetime.now()
        
        texto = render_template('email_pago.html', vivienda=vivienda,
                                pago=pay, inquilino=inquilino,
                                propietario=propietario, usuario=usuario,
                                fecha=fecha)
        for x in correos:
            data1 = {'correos': x, 'texto': texto}
            send_mail.delay(data1)

        url = 'http://alquiler:5000/api/pago'
        response = requests.post(url, json=data)
        mensaje = response.json()
        return redirect(url_for('.ver_vivienda', indice=viv_indice))

    precio = session['vivienda']['precio']
    form.cantidad.data = int(precio)
    form.nota.data = 'Sin Comentarios'

    return render_template('inquilino.html',
                           form=form,
                           precio=precio,
                           )


@alquiler_blueprint.route('/vivienda/inquilino/pago/<indice>',
                          methods=['GET', 'POST'])
def ver_pago(indice):
    """ Descarga en formato pdf la factura o comprobante de pago
        Argumentos:
            pago:  Es el id del pago realizadoque buscara en la BD.
        Return: devuelve un render template en formato pdf descargable.

    """
    # logger.info(session['pagos'])
    indice = (int(indice))
    pago = session['pagos'][indice]
    flash('pago visualizado satisfactoriamente', category='alert')
    inquilino = session['inquilino']
    vivienda = session['vivienda']
    propietario = session['propietario']
    fecha = time.strftime("%x %X")
    info = f"""En fecha {date_format(pago['fecha'])} 
se pago la cantidad de {formato_dinero(pago['cantidad'])}, 
en la vivienda {vivienda['direccion']}, 
Alt: {vivienda['numero']} por concepto pago alquiler,
por parte de {inquilino['nombre'] + ' ' + inquilino['apellido']}
de forma { pago['forma_pago']} por el mes de {nombre_mes(pago['mes'])},
impreso por: '{current_user.username}'
       {fecha}
San Cristobal, Republica Dominicana."""
    qr_generator(info)
    html = render_template('pago.html', vivienda=vivienda,
                           pago=pago, inquilino=inquilino,
                           propietario=propietario)
    return render_pdf(HTML(string=html), download_filename='pago.pdf')


@alquiler_blueprint.route('/vivienda/propietario/pago/<indice>',
                          methods=['GET', 'POST'])
def ver_pago_propietario(indice):
    """ Descarga en formato pdf la factura o comprobante de pago
        Argumentos:
            pago:  Es el id del pago realizadoque buscara en la BD.
        Return: devuelve un render template en formato pdf descargable.

    """
    # logger.info(session['pagos'])
    indice = -(int(indice))
    pago = session['prop_pagos'][indice]
    flash('pago visualizado satisfactoriamente', category='alert')
    propietario = session['propietario']
    usuario = current_user
    fecha = time.strftime("%x %X")
    info = f"""En fecha {date_format(pago['fecha'])} 
se pago la cantidad de {formato_dinero(pago['cantidad'])}, 
de forma { pago['forma_pago']} por el mes de {nombre_mes(pago['mes'])},
a {propietario['nombre'] + ' ' + propietario['apellido']},
impreso por: '{current_user.username}'
       {fecha} 
San Cristobal, Republica Dominicana.  
        """
    qr_generator(info)
    html = render_template('pago.html',
                           pago=pago, usuario=usuario,
                           propietario=propietario, fecha=fecha)
    return render_pdf(HTML(string=html), download_filename='pago.pdf')


@alquiler_blueprint.route('/vivienda/visualizacion/inquilino/',
                          methods=['GET', 'POST'])
def inquilino_visualizacion():
    form = VisualForm()
    content = None
    if form.validate_on_submit():
        cedula = form.cedula.data
        data = {'cedula': cedula}
        url = 'http://alquiler:5000/api/inquilino'
        content = requests.get(url, json=data)
        content = content.json()
        if content.get('vivienda'):
            propietario = content.get('propietario')
            vivienda = content.get('vivienda')
            inquilino = content.get('vivienda').get('inquilino')
            pagos = content.get('vivienda').get(
                'inquilino').get('pagos')
            pagos.reverse()
            session['vivienda'] = vivienda
            session['pagos'] = pagos
            session['inquilino'] = inquilino
            session['propietario'] = propietario
            
        return render_template('visual_index.html',
                               form=form, content=content)

    return render_template('visual_index.html',
                           form=form, content=content)


############### Creacion De Modelos ######################

@alquiler_blueprint.route('/crea_propietario/', methods=['GET', 'POST'])
@login_required
@has_role(['admin', 'gestor'])
def crea_propietario():
    form = PropietarioForm()
    if current_user.has_role('gestor'):
        form.usuario.choices = [(str(current_user.id), current_user.username)]
    elif current_user.has_role('admin'):
        form.usuario.choices = [(str(user.id), user.username)
                                for user in User.objects()
                                if user.has_role('gestor') or
                                user.has_role('propietario')]

    usuario = current_user.username
    if request.method == 'POST':
        if form.validate():
            # logger.info(form)
            nombre = form.nombre.data
            apellido = form.apellido.data
            alias = form.alias.data
            cedula = form.cedula.data
            usuario = {"_id": {"$oid": form.usuario.data}}
            telefono = form.telefono.data
            pp_pago = form.pp_pago.data
            email = form.email.data
            data = {'nombre': nombre,
                    'apellido': apellido,
                    'alias': alias,
                    'telefono': telefono,
                    'cedula': cedula,
                    'usuario': usuario,
                    'email': email,
                    'pp_pago': pp_pago}
            url = 'http://alquiler:5000/api/propietario'
            response = requests.post(url, json=data)
            mensaje = response.json()
            flash(f"Creado usuario", category='success')
            return redirect(url_for('.admin_home'))
    # logger.info(f"{form.usuario.choices}")
    return render_template('crear.html', form=form, usuario=usuario)


@alquiler_blueprint.route('/propietario/crea_vivienda/',
                          methods=['GET', 'POST'])
@login_required
@has_role(['admin', 'gestor'])
def crea_vivienda():
    form = ViviendaForm()
    usuario = current_user.username
    propietario = session.get('propietario')
    if form.validate_on_submit():
        direccion = form.direccion.data
        numero = form.numero.data
        precio = form.precio.data
        usuario = session['propietario']['_id']['$oid']
        data = {'direccion': direccion,
                'numero': numero,
                'precio': precio,
                'propietario': usuario
                }
        url = 'http://alquiler:5000/api/vivienda'
        response = requests.post(url, json=data)
        vivienda = response.json()
        # logger.info(session['propietario'].keys())
        indice = session['prop_indice']
        return redirect(url_for('.ver_propietario', indice=indice,
                                ))

    return render_template('crear.html', form=form,
                           propietario=propietario,
                           usuario=usuario)


@alquiler_blueprint.route('/vivienda/crea/inquilino/',
                          methods=['GET', 'POST'])
@login_required
@has_role(['admin', 'gestor'])
def crea_inquilino():
    form = InquilinoForm()
    vivienda = session.get('vivienda')
    propietario = session.get('propietario')
    if form.validate_on_submit():
        propietario = session['propietario']
        viv_indice = session['viv_indice']
        nombre = form.nombre.data
        apellido = form.apellido.data
        cedula = form.cedula.data
        telefono = form.telefono.data
        email = form.email.data
        fecha_entrada = form.fecha_entrada.data.strftime('%d-%m-%y')
        data = {'nombre': nombre,
                'apellido': apellido,
                'telefono': telefono,
                'cedula': cedula,
                'fecha_entrada': fecha_entrada,
                'email': email,
                'propietario': propietario,
                'viv_indice': viv_indice,
                }
        url = 'http://alquiler:5000/api/inquilino'
        response = requests.post(url, json=data)
        mensaje = response.json()
        return redirect(url_for('.ver_vivienda', indice=session['viv_indice']))
    return render_template('crear.html',
                           form=form,
                           vivienda=vivienda,
                           propietario=propietario,)


####################### Elimina Modelos ##################################


@alquiler_blueprint.route('/vivienda/inquilino/elimina/pago/<indice>',
                          methods=['GET'])
@login_required
@has_role(['admin', 'gestor'])
def elimina_pago(indice):
    # niego el indice para eliminar el pago correcto pues al invertir el indice comienza en 1.
    indice = -(int(indice))
    viv_indice = session.get('viv_indice')
    propietario = session.get('propietario')
    dicto = {'pago_indice': indice, 'viv_indice': viv_indice,
             'propietario': propietario}
    url = 'http://alquiler:5000/api/pago'
    data = requests.delete(url, json=dicto)
    flash(data.json()['mensaje'], category="info")
    return redirect(url_for('.ver_vivienda',
                            indice=session.get(
                                'viv_indice')))


@alquiler_blueprint.route('/vivienda/propietario/elimina/pago/<indice>',
                          methods=['GET'])
@login_required
@has_role(['admin', 'gestor'])
def elimina_pago_propietario(indice):
    # niego el indice para eliminar el pago correcto pues al invertir el indice comienza en 1.
    indice = -(int(indice))
    prop_indice = session.get('prop_indice')
    propietario = session.get('propietario')
    dicto = {'pago': indice,
             'propietario': propietario}
    url = 'http://alquiler:5000/api/propietario'
    data = requests.delete(url, json=dicto)
    flash(data.json()['mensaje'], category="info")
    return redirect(url_for('.ver_propietario',
                            indice=prop_indice))


@alquiler_blueprint.route('/vivienda/elimina/vivienda',
                          methods=['GET'])
@login_required
@has_role(['admin', 'gestor'])
def eliminar_vivienda():
    url = 'http://alquiler:5000/api/vivienda'
    propietario = session['propietario']
    vivienda_indice = session['viv_indice']
    dicto = {'propietario': propietario, 'indice': vivienda_indice}
    data = requests.delete(url, json=dicto)
    flash(data.json()['mensaje'], category="info")
    return redirect(url_for('.ver_propietario', indice=session['prop_indice']))


@alquiler_blueprint.route('/vivienda/vacear/vivienda/',
                          methods=['GET'])
@login_required
@has_role(['admin', 'gestor'])
def vacear_vivienda():
    viv_indice = session.get('viv_indice')
    propietario = session.get('propietario')
    mudanza = True
    url = 'http://alquiler:5000/api/vivienda'
    data = {'viv_indice': viv_indice, 'mudanza': mudanza,
            'propietario': propietario}
    response = requests.put(url, json=data)
    flash(response.json()['mensaje'], category="info")
    return redirect(url_for('.ver_vivienda',
                            indice=session.get(
                                'viv_indice')))


@alquiler_blueprint.route('/inquilino/eliminar_nota/<indice>')
@login_required
@has_role(['admin', 'gestor'])
def elimina_nota_propietario(indice):
    indice = -(int(indice))
    propietario = session.get('propietario')
    dicto = {'propietario': propietario, 'nota': indice}
    url = 'http://alquiler:5000/api/propietario'
    data = requests.delete(url, json=dicto)
    flash(data.json()['mensaje'], category="info")
    return redirect(url_for('.ver_propietario', indice=session['prop_indice']))


@alquiler_blueprint.route('/vivienda/eliminar_nota/<indice>')
@login_required
@has_role(['admin', 'gestor'])
def elimina_nota_vivienda(indice):
    indice = -(int(indice))
    propietario = session.get('propietario')
    vivienda = session.get('viv_indice')
    dicto = {'propietario': propietario, 'nota': indice, "vivienda": vivienda}
    url = 'http://alquiler:5000/api/vivienda'
    data = requests.delete(url, json=dicto)
    flash(data.json()['mensaje'], category="info")
    return redirect(url_for('.ver_vivienda', indice=session['viv_indice']))


@alquiler_blueprint.route('/propietario/elimina/',
                          methods=['GET'])
@login_required
@has_role(['admin', 'gestor'])
def elimina_propietario():
    url = 'http://alquiler:5000/api/propietario'
    propietario = session['propietario']
    dicto = {'propietario': propietario, 'self': True}
    data = requests.delete(url, json=dicto)
    flash(data.json()['mensaje'], category="info")
    return redirect(url_for('.admin_home'))


# Custom Filters

def date_format(value, formato="%d-%B-%y"):
    if isinstance(value, dict):
        value = value['$date']
        fecha = time.strftime(formato, time.localtime(value/1000))
    elif isinstance(value, datetime.datetime):
        fecha = value.strftime(formato)
    return fecha


def nombre_mes(value):
    meses = {1: 'Enero', 2: 'Febrero', 3: 'Marzo',
             4: 'Abril', 5: 'Mayo', 6: 'Junio',
             7: 'Julio', 8: 'Agosto', 9: 'Septiembre',
             10: 'Octubre', 11: 'Noviembre', 12: 'Diciembre'}
    if isinstance(value, int):
        return meses[value]
    elif isinstance(value, str):
        return meses[int(value)]


def formato_dinero(valor):
    valor = float(valor)
    value = f'RD$ {valor:.2f}'
    return value


jinja2.filters.FILTERS['date_format'] = date_format
jinja2.filters.FILTERS['nombre_mes'] = nombre_mes
jinja2.filters.FILTERS['formato_dinero'] = formato_dinero
