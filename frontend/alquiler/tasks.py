from .. import celery
from flask import render_template, session, current_app
import smtplib
from email.mime.text import MIMEText



@celery.task()
def log(msg):
    return msg


@celery.task(bind=True,
             ignore_result=True,
             default_retry_delay=10,
             max_retries=5)
def send_mail(self, datos):
    mensaje = MIMEText(datos['texto'], 'html')
    mensaje['FROM'] = current_app.config['SMTP_FROM']
    mensaje['SUBJECT'] = "Se Acaba De Realizar Un PAgo"
    mensaje['TO'] = datos['correos']


    try:
        smtp_server = smtplib.SMTP(current_app.config['SMTP_SERVER'], 587)
        smtp_server.ehlo()
        smtp_server.starttls()
        smtp_server.login(current_app.config['SMTP_USER'],
                          current_app.config['SMTP_PASSWORD'])
        smtp_server.ehlo()
        smtp_server.sendmail(current_app.config['SMTP_FROM'],
                             datos['correos'], mensaje.as_string())
        smtp_server.close()
        return
    except Exception as e:
        self.retry(exc=e)
