import datetime
from collections import Counter



mes = {1: 'enero', 2: 'febrero', 3: 'marzo', 4: 'abril',
       5: 'mayo', 6: 'junio', 7: 'julio', 8: 'agosto',
       9: 'septiembre', 10: 'octubre', 11: 'nobiembre',
       12: 'diciembre'}

ftm = '%d-%m-%y'


def procesa_datos(barra_lateral):
    """Funcion para procesar los datos de la barra lateral
    para disminuir el calculo y los errores en el template
    Toma como parametro el diccionario barra lateral creada 
    en alquiler_api y calcula los datos de la barra
    """
    mes_actual = datetime.datetime.now().month  # mes actual de calculo
    mes_anterior = mes_actual - 1               # mes anterior
    total_meses = {}                            # cantidad cobrada por mes
    conteo_pago_meses = []                      # cantidad alquileres cobrados
    falta_cobrar = {}                           # alquileres faltantes meses
    total_meses_descuento = {}                  # total meses con descuento
    pago_cobrador = {}                          # pago que recibira el cobrador
    
    numero_alquiler = barra_lateral.get('numero_alquiler')
    mensualidad_bruta = barra_lateral.get('mensualidad_bruta')
    mensuallidad_neta = barra_lateral.get('mensualidad_neta')
    deduccion_cobros = barra_lateral.get('deduccion_cobros')
    pagos_propietario = []
    if barra_lateral.get('mensualidades'):
        for item in barra_lateral['mensualidades']['total']:
            total_meses.update(barra_lateral.get('mensualidades').get('total'))
    
        for item in barra_lateral['mensualidades']['lista']:
            conteo_pago_meses = dict(Counter([y for x in item for y in x]))

        falta_cobrar = {
            item: abs(total_meses[item] - barra_lateral['mensualidad_bruta'])
            for item in total_meses}
        
        total_meses_descuento = {
           item: abs(total_meses[item] + (- (conteo_pago_meses[item] * 500)))
            for item in total_meses
        }
        
        for item in total_meses_descuento:
            if barra_lateral['pagos_propietario'].get(item):
                pagos_propietario = barra_lateral['pagos_propietario']
                total_meses_descuento[item] = (
                    barra_lateral['pagos_propietario'].get(item)['pagado']
                    - total_meses_descuento[item]
                )

        pago_cobrador = {
            item: conteo_pago_meses[item] * 500
            for item in conteo_pago_meses
        }

    procesado = {
        'mes_actual': str(mes_actual),
        'mes_anterior': str(mes_anterior),
        'total_meses': total_meses,
        'conteo_pago_meses': conteo_pago_meses,
        'falta_cobrar': falta_cobrar,
        'total_meses_descuento': total_meses_descuento,
        'pago_cobrador': pago_cobrador,
        'numero_alquiler': numero_alquiler,
        'mensualidad_bruta': mensualidad_bruta,
        'mensualidad_neta':  mensuallidad_neta,
        'deduccion_cobro': deduccion_cobros,
        'pagos_propietario': pagos_propietario or None,
        
    }
    return procesado


    """
    {'detalle_inquilinos': [{'7': {'pagado': 10000, 'veces': 2}, 'id': '5f22ccf327f6a2931cd42d03', 'nombre': 'Luis'}], 

    'mensualidades': {'cantidad': 2, 'lista': [[{'7': 5000}, {'7': 5000}]], 'total': {'7': 10000}}, 

    'pagos_propietario': {'id': '5f22ca61ff2f4e3097142676', 'nombre': 'enerido alejandro'}, '

    numero_alquiler': 1, 

    'mensualidad_bruta': 5000.0, 

    'mensualidad_neta': 4500.0, 
    
    'deduccion_cobros': 5000
    """

def proceso_pagos(pagos):
    pagos1 = []
    for z in pagos:
        dictio = {}
        for x, y in z.items():
            if x == "fecha":
                timeri = datetime.datetime.fromtimestamp(y['$date'] / 1000)
                dictio[x] = timeri.strftime(ftm)
            elif x == 'mes':
                dictio['nombre_mes'] = mes.get(y)
                dictio['mes'] = y
            else:
                dictio[x] = y
        pagos1.append(dictio)
        pagos1.reverse()
    return pagos1


