import datetime
from flask_wtf import FlaskForm as Form
from flask_wtf import recaptcha
from wtforms import (StringField, BooleanField,
                     TextAreaField, SelectField,
                     IntegerField, FloatField, TextAreaField)
from wtforms.validators import DataRequired, Length, Email
from ..auth.models import User
from wtforms.fields.html5 import EmailField, DateField, TelField 


class PropietarioForm(Form):
    nombre = StringField('Nombre', [DataRequired()])
    apellido = StringField('Apellido', [DataRequired()])
    usuario = SelectField('Usuario', [DataRequired()], choices=[])
    

    email = EmailField('Email',
                       [DataRequired('Por favor introduce un email'),
                        Email('Por favor introduce un Email Valido')])
    alias = StringField('Alias', [DataRequired()])
    cedula = StringField('Cedula', [DataRequired(), Length(min=11, max=11)])
    telefono = TelField('telefono', [DataRequired()])
    pp_pago = FloatField("PagoxCobro", [DataRequired()])
        

class ViviendaForm(Form):
    direccion = StringField('Direccion', [DataRequired()])
    numero = StringField('Numero', [DataRequired()])
    precio = IntegerField('Mensualidad', [DataRequired()])


class InquilinoForm(Form):
    nombre = StringField('Nombre',
                         [DataRequired()])
    apellido = StringField('Apellido',
                           [DataRequired()])
    cedula = StringField('Cedula',
                         [DataRequired(),
                          Length(min=11, max=11)])
    telefono = TelField('Telefono',
                        [DataRequired()])
    fecha_entrada = DateField('Fecha Entrada',
                              # format='%d-%m-%y', se elimina formato para poder validar
                              validators=[DataRequired()])
    email = EmailField('Email',
                       [Email('Por favor introduce un Email Valido')])


class PagoForm(Form):
    cantidad = IntegerField('Cantidad',
                            [DataRequired()]
                            )
    fecha = datetime.datetime.now()
    mes = SelectField('Mes',
                      [DataRequired()],
                      choices=[(1, 'enero'), (2, 'febrero'), (3, 'Marzo'),
                               (4, 'abril'), (5, 'mayo'), (6, 'junio'),
                               (7, 'julio'), (8, 'agosto'), (9, 'septiembre'),
                               (10, 'octubre'), (11, 'noviembre'), (12, 'diciembre')],
                      default=fecha.month, coerce=int,)
    forma_pago = SelectField("Forma De Pago",
                             [DataRequired()],
                             choices=[('efectivo', 'Efectivo'),
                                      ('transferencia', 'Transferencia')]
                             )
    nota = TextAreaField("Comentario")


class NotaForm(Form):
    comentario = TextAreaField('Contenido', [DataRequired(), Length(min=15)])

class VisualForm(Form):
    cedula = StringField('cedula',
                         [DataRequired(
                             'Datos Requeridos para identificaral inquilino.'
                         ), Length(min=11, max=11)])
