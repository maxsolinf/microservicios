
def create_module(app):
    from .controllers import alquiler_blueprint

    app.register_blueprint(alquiler_blueprint)
