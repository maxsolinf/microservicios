from .controllers import session
import datetime

dias_mes = datetime.timedelta(days=29)
mes_actual = datetime.datetime.now()
mes_calcular = mes_actual - dias_mes

def owner(result):
    propietario = result.get('propietario')
    informacion = result.get('informa')
    viviendas = propietario.get('viviendas')
    notas = propietario.get('notas')
    pagos = propietario.get('pagos')
    if pagos:
        pagos.reverse()
    if notas:
        notas.reverse()
    session['prop_pagos'] = pagos
    session['prop_notas'] = notas
    session['propietario'] = propietario
    if viviendas:
        session['viviendas'] = viviendas
    debitar = 1
    if informacion.get('prevision'):
        if propietario.get('pp_pago') <= 50:
            debitar = (session.get('propietario').get('pp_pago')
                       / 100) * informacion.get('prevision'
                                                ).get('dinero_esperado')
        elif propietario.get('pp_pago') > 100:
            debitar = (session.get('propietario').get('pp_pago') *
                       informacion.get('prevision').get('vivienda_ocupadas'))
    informacion['debitar'] = debitar
    informacion['mes_actual'] = str(mes_calcular.month)
    return (viviendas, notas, pagos, informacion, propietario)

def casa(resultado):
    informacion = resultado.get('informa')
    vivienda = resultado.get('vivienda')
    inquilino = vivienda.get('inquilino')
    if inquilino:
        pagos = inquilino.get('pagos')
        # revierto los pagos para visualizar en orden cronologico inverso
        pagos.reverse()
        session['pagos'] = pagos
        session['inquilino'] = inquilino
        # logger.info(f'pagos son {session["pagos"]}')
        # logger.info(f'inquilino son {session["inquilino"].keys()}')
    else:
        session['pagos'] = []
        session['inqilino'] = []
    session['vivienda'] = vivienda
    return informacion, vivienda, inquilino
    
