from flask import Flask, render_template, flash
from flask_mongoengine import MongoEngine
from celery import Celery
import jinja2


def acceso_denegado(e):
    flash('Acceso denegado', category='danger')
    mensaje = "Lo sentimos pero no tienes acceso a esta zona de la pagina"
    return render_template('403.html', mensaje=mensaje), 403


mongo = MongoEngine()
celery = Celery(__name__)


def create_app(objector):
    app = Flask(__name__)
    app.config.from_object(objector)
    app.register_error_handler(403, acceso_denegado)

    mongo.init_app(app)
    celery.conf.broker_url = app.config.get('CELERY_BROKER_URL')
    celery.conf.result_backend = app.config.get('CELERY_RESULT_BACKEND')
    celery.autodiscover_tasks(['frontend.alquiler.tasks'])
    celery.conf.update(app.config)
    from .auth import create_module as auth_create_module
    from .blog import create_module as blog_create_module
    from .main import create_module as main_create_module
    from .alquiler import create_module as alquiler_create_module

    main_create_module(app)
    auth_create_module(app)
    blog_create_module(app)
    alquiler_create_module(app)
    return app
