def create_module(app):
    from .controller import main_blueprint
    app.register_blueprint(main_blueprint)
