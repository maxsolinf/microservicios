from flask import Blueprint, render_template

main_blueprint = Blueprint(
    'main',
    __name__,
    url_prefix='/',
    template_folder='../templates/auth',
    static_folder='../static',)


@main_blueprint.route('/')
def front_page():
    return render_template('index.html')
