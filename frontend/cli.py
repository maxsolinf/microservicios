import logging
import click
from .auth.models import User, Role

log = logging.getLogger(__name__)

def register(app):
    @app.cli.command('crea-usuario')
    @click.argument('username')
    @click.argument('password')
    def crea_usuario(username, password):
        user = User()
        user.username = username
        user.set_password(password)
        try:
            user.save()
            click.echo(f'Usuario "{user.username}" agregado.')
        except Exception as e:
            log.error(f"Fallo al agregar usuario {user.username}: error: {e}")

    @app.cli.command('agrega-role')
    @click.argument('username')
    @click.argument('roler')
    def agrega_role(username, roler):
        user = User.objects(username=username).first()
        role = Role.objects(nombre=roler).first()
        if user:
            if role:
                if not user.has_role(roler):
                    user.roles.append(role)
                    user.save()
                    click.echo(f'Role: {role} agregado satisfactoriamente a {username}')
                    return
                else:
                    log.error(f"El usuario ya posee ese role: {roler}")
                    return
            else:
                log.error(f'El role que intentas agregar no existe {roler}, crea el role antes de agregarlo.')
                return
        else:
            log.error(f'El Usuario {username} no existe en la base de datos crealo antes de agregar role.')
            return

    @app.cli.command('crea-role')
    @click.argument('role')
    def crea_role(role):
        roler = Role.objects(nombre=role)
        if not roler:
            roler = Role(nombre=role)
            try:
                roler.save()
                click.echo(f'Role: "{role}" agregado')
                return
            except Exception as e:
                log.error(f'No se pudo agregar Role error: {e}')
                return
        else:
            log.error(f"Rol: '{role}' ya existe en la base de datos")
            return
