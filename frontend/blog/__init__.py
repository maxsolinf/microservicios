def create_module(app):
    from .controllers import blog_blueprint

    app.register_blueprint(blog_blueprint)
