from flask_wtf import FlaskForm as Form
from flask_wtf import recaptcha
from wtforms import StringField, BooleanField, TextAreaField
from wtforms.validators import DataRequired, Length


class PostForm(Form):
    title = StringField('Titulo', [DataRequired(), Length(min=8)])
    content = TextAreaField("Contenido", [DataRequired(), Length(min=30)])
    tags = StringField("Etiquetas", [DataRequired(), Length(min=5, max=150)])


class CommentForm(Form):
    content = TextAreaField('Comentario', [DataRequired()])
