import datetime
from werkzeug.local import LocalProxy
import requests
from flask_login import login_required
from flask import (Blueprint,
                   redirect,
                   url_for,
                   flash,
                   render_template,
                   request, current_app,
                   session,)

from ..auth import current_user, has_role, AnonymousUser
from ..auth.models import User
from .forms import CommentForm, PostForm


blog_blueprint = Blueprint(
    'blog',
    __name__,
    url_prefix='/blog',
    template_folder='../templates/blog',
    static_folder='../static/'
)

logger = LocalProxy(lambda: current_app.logger)

def side_bar():
    """Aqui buscamos las etiquetas mas usadas y los post mas recientes
    desde la api"""
    pass


@blog_blueprint.route('/', methods=['GET'])
def blog_home():
    data = requests.get('http://blog:5000/api')
    contenido = data.json()
    logger.info(contenido)
    now = datetime.datetime.now()
    return render_template('home.html',
                           contenido=contenido,
                           now=now,)


@blog_blueprint.route('/<post_id>', methods=['GET', 'POST'])
def ver_blog(post_id):
    form = CommentForm()
    if form.validate_on_submit():
        if current_user.is_authenticated:
            data = {'user': str(current_user.id), 'comment': form.content.data}
            url = f'http://blog:5000/api/comment/{post_id}'
            response = requests.post(url, json=data)
            return redirect(url_for('.ver_blog', post_id=post_id))

        flash('Debes estar logeado para poder comentar.', category='info')
        return redirect(url_for('.ver_blog', post_id=post_id))

    data = requests.get(f'http://blog:5000/api/{post_id}')
    contenido = data.json()
    if contenido.get('post'):
        session['contenido'] = contenido
        print('********** aqui el contenido', contenido)
        return render_template('post.html',
                               contenido=contenido,
                               form=form, post_id=post_id)

    flash('Post no encontrado', category='danger')
    return redirect(url_for('.blog_home'))


@blog_blueprint.route('/crea_post', methods=['GET', 'POST'])
@login_required
@has_role('poster')
def crea_post():
    """Al postear en la raiz creamos una nueva entrada en el blog
    para que sea funcional recuerda colocar el login_required, has_role
    y tambien enviarlo a la api blog"""

    form = PostForm()
    if form.validate_on_submit():
        data = {'title': form.title.data,
                'content': form.content.data,
                'tags': form.tags.data,
                'user': str(current_user.id)}
        url = 'http://blog:5000/api'
        response = requests.post(url, json=data)
        data_response = response.json()
        if 'post_id' in data_response:
            logger.info(data_response)
            flash(data_response['mensaje'], category='success')
            return redirect(url_for('.ver_blog',
                                    post_id=data_response['post_id']))
    flash('Aqui puedes crear tus Post.', category='info')
    return render_template('crea_post.html',
                           form=form)


@blog_blueprint.route('/edit/<post_id>', methods=['GET', 'POST'])
@login_required
def edit_blog(post_id):
    """Aqui capturas las modificAciones Al Blog En Un Json Y Lo Envias 
    De Vuelta A La Api Para Que Actualice La Base De Datos"""
    form = PostForm()
    if request.method == 'POST' and form.validate():
        data = {'title': form.title.data,
                'content': form.content.data,
                'tags': form.tags.data,
                'user': str(current_user.id)}
        url = f'http://blog:5000/api/{post_id}'
        response = requests.put(url, json=data)
        data_response = response.json()
        flash(data_response['mensaje'], category='success')
        return redirect(url_for('.ver_blog',
                                post_id=data_response['post_id']))

    data = requests.get(f'http://blog:5000/api/{post_id}')
    blog = data.json()
    form.title.data = blog['post']['title']
    form.content.data = blog['post']['content']
    form.tags.data = ','.join(blog['post']['tags'])
    if current_user.username == blog['username']:
        flash('Puedes comenzar a editar tu post.', category='info')
        return render_template(
            'edit_post.html',
            form=form,
            post_id=post_id)
    flash('Solo los autores de los sus post pueden editarlos.', category="danger")
    return redirect(url_for(".blog_home"))


@blog_blueprint.route('/delete/<blog_id>', methods=['GET'])
@login_required
@has_role('poster')
def delete_blog(blog_id):
    """Aqui capturas las modificaciones al blog en un json y lo envias 
    de vuelta a la api para que actualice la base de datos"""
    if current_user.is_authenticated:
        if current_user.username == session.get(
                'contenido').get('username') or current_user.has_role('admin'):
            
            userid = str(current_user.id)
            roles = []
            for role in current_user.roles:
                roles.append(role.nombre)
                data = {'userid': userid, 'roles': roles}
                url = f'http://blog:5000/api/{blog_id}'
                del_resp = requests.delete(url, json=data)
                respuesta = del_resp.json()
                print(respuesta)
                flash(respuesta['mensaje'], category='alert')
                return redirect(url_for('.blog_home'))
    flash('No se puede eliminar posts si no eres el propietario del post',
          category='danger')
    return redirect(url_for('.blog_home'))


@blog_blueprint.route('/tag/<tag_name>')
def busca_etiquetas(tag_name):
    url = f'http://blog:5000/api/tag/{tag_name}'
    data = requests.get(url)
    contenido = data.json()
    now = datetime.datetime.now()
    if contenido.get('posts'):
        flash(
            f'Etiqueta "{tag_name}" encontrada, estos son las entradas con esa etiqueta.', category="success")
        return render_template('home.html',
                               contenido=contenido,
                               now=now,)
    flash('Tag no encontrada', category="danger")
    return redirect(url_for('.blog_home'))


@blog_blueprint.route('/user/<user_name>')
def user_post(user_name):
    user = User.objects(username=user_name).first()
    if user:
        url = f'http://blog:5000/api/user/{str(user.id)}'
        data = requests.get(url)
        contenido = data.json()
        now = datetime.datetime.now()
        if contenido.get('posts'):
            flash(
                f'Usuario "{user_name}" encontrado, estos son los post de este usuario', category="success")
            return render_template('home.html',
                                   contenido=contenido,
                                   now=now,)
        flash('Publicacion de usuario no encontrada.', category="danger")
        return redirect(url_for('.blog_home'))
