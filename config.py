import os
from datetime import timedelta

"""Buscamos las Variables De Entorno y Configuramos aplicacion """
if os.environ.get('SECRET_KEY'):
    secret = os.environ.get('SECRET_KEY')
else:
    secret = '\xce\x1cE@\x06\xd4Cr\x87g\xf6Y\xb3U\xdb\x7f\x9c\xcb\x92\xd0H\xf2\xd8\xfc\xf3\x07~A\xe7Ro\xcb\xb8`vz\x8d\x02m^\xd2\x97z\xef'

mongo = {'db': 'prueba', 'host': '192.168.0.150', 'port': 10001, }

for param in mongo.keys():
    if param == 'port':
        mongo[param] = int(os.environ.get(param.upper(), 10001))
    elif os.environ.get(param.upper()):
        mongo[param] = os.environ.get(param.upper())
    else:
        continue
recaptcha_public = os.environ.get(
    'RECAPTCHA_PUBLIC_KEY', '6LckBfsUAAAAAK7vrISM9-4uTSa0EOWibx-Vr9ix')
recaptcha_private = os.environ.get(
    'RECAPTCHA_PRIVATE_KEY', '6LckBfsUAAAAALHye7kFrTec7kcQiYqgmToVGuh7')


class Config(object):
    """ Recordar configurar la secret key mediante variable de entorno para
        mayor seguridad, las variables de entorno serian 
    Variables Entorno:
        SECRET_KEY:
        DB:     Base de datos a la Cual conectaremos los modelos.
        HOST:   Host Mongodb para coneccion DB.
        PORT:   Puerto Mongodb para coneccion DB.
        DEBUG:  (True or False) Modo Depuracion False para produccion.
    """
    DEBUG = not bool(os.environ.get('DEBUG', False))
    RECAPTCHA_PUBLIC_KEY = recaptcha_public
    RECAPTCHA_PRIVATE_KEY = recaptcha_private
    CELERY_BROKER_URL = "amqp://rabbitmq:rabbitmq@rabbitmq//"
    CELERY_RESULT_BACKEND = "amqp://rabbitmq:rabbitmq@rabbitmq//"
    SMTP_SERVER = os.environ.get("SMTP_SERVER", "smtp.gmail.com")
    SMTP_FROM = os.environ.get("SMTP_FROM")
    SMTP_USER = os.environ.get("SMTP_USER")
    SMTP_PASSWORD = os.environ.get("SMTP_PASSWORD")
    SESSION_PERMANENT = True
    SESSION_TYPE = 'filesystem'
    PERMANENT_SESSION_LIFETIME = timedelta(hours=5)
    SESSION_FILE_THRESHOLD = 100
    SECRET_KEY = secret


class DevConfig(Config):

    MONGODB_SETTINGS = mongo
