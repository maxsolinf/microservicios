from flask_session import Session
from frontend import create_app
import os
from frontend.cli import register

if not os.environ.get('FRONTEND_CONFIG'):
    os.environ['FRONTEND_CONFIG'] = 'DevConfig'
direct = os.path.curdir
config_dir = f'config.{os.environ["FRONTEND_CONFIG"]}'

sess = Session()
app = create_app(config_dir)

sess.init_app(app)
register(app)

if __name__ == '__main__':
    app.run(host='0.0.0.0')
